# CSS Framework

## Introduction
This is just my CSS library that I created for freelance products to speed up my development time and simplify my coordinations with designers I work with.

### CDN
CDN: https://unpkg.com/michaelmarkie-css_framework

### Building
Build requires Node.js and NPM
```bash
npm install
npm run build
```

### Grid System
12 column now based on CSS grid, simplified the code massivly and easier to use over flexbox.

The following will stretch over 4 column's
```html
<!-- c12 = 12 columns (options: 12, 24), w960 = 960 width (optional), g30 = 30px gutter (default: 15, options: 20, 25, 30) -->
<div class="fluid_container c12 w960 g30">
    <div class="fluid_4"></div>
</div>
```