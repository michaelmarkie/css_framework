const { src, dest, series, parallel, watch } = require('gulp');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const rename = require('gulp-rename');


function distTask(cb) {
    src('src/sass/core.sass')
		.pipe(sass().on('error', console.error))
		.pipe(rename('framework.css'))
		.pipe(dest('dist'));

	src('src/sass/core.sass')
		.pipe(sass({ outputStyle: 'compressed' }).on('error', console.error))
		.pipe(rename('framework.min.css'))
		.pipe(dest('dist'));
		
	cb();
}

function sassTask(cb) {
    src('src/sass/core.sass', { sourcemaps: true })
		.pipe(sass().on('error', console.error))
		.pipe(dest('public', { sourcemaps: '.' }));
		
	cb();
}
function watchSassTask(cb) {
	watch('src/sass/*.*', sassTask);

	cb();
}
function pugTask(cb) {
	src('src/**/*.pug')
		.pipe(pug({ pretty: true }))
		.pipe(dest('public'));
	
	cb();
}
function watchPugTask(cb) {
	watch('src/*.pug', pugTask);

	cb();
}

exports.sass = sassTask;
exports.pug = pugTask;
exports.dist = distTask;

exports.build = series(sassTask,pugTask);
exports.watch = parallel(watchSassTask,watchPugTask);
exports.default = series(sassTask,pugTask);